<?php

namespace App\Repository;

use App\Entity\Image;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Image|null find($id, $lockMode = null, $lockVersion = null)
 * @method Image|null findOneBy(array $criteria, array $orderBy = null)
 * @method Image[]    findAll()
 * @method Image[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ImageRepository extends ServiceEntityRepository
{
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * ImageRepository constructor.
     * @param ManagerRegistry $registry
     * @param EntityManagerInterface $manager
     */
    public function __construct(ManagerRegistry $registry, EntityManagerInterface $manager)
    {
        parent::__construct($registry, Image::class);
        $this->manager = $manager;
    }

    /**
     * @param string $title
     * @param string $description
     * @param string $category
     * @param string $url
     */
    public function saveImage(string $title, string $description, string $category, string $url)
    {
        $image = new Image();

        $image
            ->setTitle($title)
            ->setDescription($description)
            ->setCategory($category)
            ->setUrl($url);

        $this->manager->persist($image);
        $this->manager->flush();
    }

    /**
     * @param Image $image
     * @return Image
     */
    public function updateImage(Image $image): Image
    {
        $this->manager->persist($image);
        $this->manager->flush();

        return $image;
    }

    /**
     * @param Image $image
     */
    public function removeImage(Image $image)
    {
        $this->manager->remove($image);
        $this->manager->flush();
    }

    /**
     * @param array $dataImage
     */
    public function saveImageMultiple(array $dataImage)
    {
        $imageAdd = false;
        foreach ($dataImage as $value) {

            if (!$this->findOneByTitle($value['title'])) {
                $image = new Image();

                $image
                    ->setTitle($value['title'])
                    ->setDescription($value['description'])
                    ->setCategory($value['category'])
                    ->setUrl($value['url']);

                $imageAdd = true;
                $this->manager->persist($image);
            }

        }
        $this->manager->flush();
        return $imageAdd;
    }

    public function findOneByTitle($value): ?Image
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.title = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult();
    }

    // /**
    //  * @return Image[] Returns an array of Image objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Image
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

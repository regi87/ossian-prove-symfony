<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserController extends  AbstractController
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * ImageController constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @Route("/api/user/register", name="user_register", methods={"POST"})
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param Request $request
     * @return JsonResponse
     */
    public function register(UserPasswordEncoderInterface $passwordEncoder, Request $request): JsonResponse
    {
        $errors = [];

        $data = json_decode($request->getContent(), true);

        $email = $data['email'];
        $password = $data['password'];
        $passwordConfirmation = $data['passwordConfirmation'];

        if ($password != $passwordConfirmation) {
            $errors[] = "Password does not match the password confirmation.";
        }
        if (strlen($password) < 6) {
            $errors[] = "Password should be at least 6 characters.";
        }
        if (!$errors) {
            $user = new User();
            $encodedPassword = $passwordEncoder->encodePassword($user, $password);
            try {
                $this->userRepository->registerUser($email, $encodedPassword);
                return new JsonResponse([
                    'user' => $user,
                    'result' => 'User Created'
                ], Response::HTTP_CREATED);
            } catch (UniqueConstraintViolationException $e) {
                $errors[] = "The email provided already has an account!";
            } catch (\Exception $e) {
                $errors[] = "Unable to save new user at this time.";
            }
        } else {
            return new JsonResponse([
                'errors' => $errors
            ], 400);
        }
    }

    /**
     * @Route("/api/user/login", name="user_login", methods={"POST"})
     */
    public function login()
    {
        return $this->json(['result' => true]);
    }

    /**
     * @Route("/api/user/profile", name="user_profile")
     * @IsGranted("ROLE_USER")
     */
    public function profile()
    {
        return $this->json([
            'user' => $this->getUser()
        ],
            200,
            [],
            [
                'groups' => ['api']
            ]
        );
    }
}

<?php

namespace App\Controller;

use App\Repository\ImageRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class ImageController
{
    /**
     * @var ImageRepository
     */
    private $imageRepository;

    /**
     * ImageController constructor.
     * @param ImageRepository $imageRepository
     */
    public function __construct(ImageRepository $imageRepository)
    {
        $this->imageRepository = $imageRepository;
    }

    /**
     * @Route("/api/image/add", name="add_image", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function add(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        $title = $data['title'];
        $description = $data['description'];
        $category = $data['category'];
        $url = $data['url'];

        if (empty($title) || empty($category) || empty($url)) {
            throw new NotFoundHttpException('Expecting mandatory parameters!');
        }

        $this->imageRepository->saveImage($title, $description, $category, $url);

        return new JsonResponse(['status' => 'Image created!'], Response::HTTP_CREATED);
    }

    /**
     * @Route("/api/image/add-multiple", name="add_image_multiple", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function addMultiple(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $isAdded = $this->imageRepository->saveImageMultiple($data['result']);

        if ($isAdded) {
            return new JsonResponse(['status' => 'Image created!'], Response::HTTP_CREATED);
        } else {
            return new JsonResponse(['status' => 'Image not added!'], Response::HTTP_CONFLICT);
        }
    }

    /**
     * @Route("/api/image/list", name="list_images", methods={"GET"})
     * @return JsonResponse
     */
    public function list(): JsonResponse
    {
        $images = $this->imageRepository->findAll();
        $data = [];
        foreach ($images as $image) {
            $data [] = [
                'id' => $image->getId(),
                'title' => $image->getTitle(),
                'description' => $image->getDescription(),
                'category' => $image->getCategory(),
                'url' => $image->getUrl(),
            ];
        }

        return new JsonResponse($data, Response::HTTP_OK);
    }

    /**
     * @Route("/api/image/get/{id}", name="get_image", methods={"GET"})
     * @param $id
     * @return JsonResponse
     */
    public function get($id): JsonResponse
    {
        $image = $this->imageRepository->findOneBy(['id' => $id]);

        $data = [
            'id' => $image->getId(),
            'title' => $image->getTitle(),
            'description' => $image->getDescription(),
            'category' => $image->getCategory(),
            'url' => $image->getUrl(),
        ];

        return new JsonResponse($data, Response::HTTP_OK);
    }

    /**
     * @Route("/api/image/update/{id}", name="update_image", methods={"PUT"})
     * @param $id
     * @param Request $request
     * @return JsonResponse
     */
    public function update($id, Request $request): JsonResponse
    {
        $image = $this->imageRepository->findOneBy(['id' => $id]);
        $data = json_decode($request->getContent(), true);

        empty($data['title']) ? true : $image->setTitle($data['title']);
        empty($data['description']) ? true : $image->setDescription($data['description']);
        empty($data['category']) ? true : $image->setCategory($data['category']);
        empty($data['url']) ? true : $image->setUrl($data['url']);

        $updatedImage = $this->imageRepository->updateImage($image);

        return new JsonResponse($updatedImage->toArray(), Response::HTTP_OK);
    }

    /**
     * @Route("/api/image/delete/{id}", name="delete_image", methods={"DELETE"})
     * @param $id
     * @return JsonResponse
     */
    public function delete($id): JsonResponse
    {
        $image = $this->imageRepository->findOneBy(['id' => $id]);

        $this->imageRepository->removeImage($image);

        return new JsonResponse(['status' => 'Image deleted'], Response::HTTP_NO_CONTENT);
    }
}
